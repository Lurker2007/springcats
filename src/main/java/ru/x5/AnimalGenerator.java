package ru.x5;

import java.util.List;

public interface AnimalGenerator {
  public List<Animal> getAnimals();
}
