package ru.x5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class CatGenerator implements AnimalGenerator{
  @Autowired
  //@Qualifier("cat")
  private Cat cat;

  private List<Animal> cats;

  public CatGenerator() {
    System.out.println("Cat Generator: constructor");
  }

  @PostConstruct
  private void constructAnimals() {
    System.out.println("Cat Generator: post construct method");
    cats = new ArrayList<Animal>();

    cats.add(cat.getAnimal());
    cats.add(cat.getAnimal());
    cats.add(cat.getAnimal());
    cats.add(cat.getAnimal());
  }

  public List<Animal> getAnimals() {
    System.out.println("Cat Generator: cat created");
    return cats;
  }
}
