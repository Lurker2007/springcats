package ru.x5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ShelterService {
  @Autowired
  private AnimalGenerator animalGenerator;
  @Autowired
  private Keeper keeper;

  public void liveInShelter() {
    List<Animal> animals = animalGenerator.getAnimals();
    for (Animal animal : animals) {
      Thread thread = new Thread(new AnimalThread(animal));
      thread.start();
    }
    Thread keeperThread = new Thread(keeper);
    keeperThread.start();
  }
}
