package ru.x5;

import lombok.Data;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@Data
public class AnimalThread implements Runnable {
  private Animal animal;
  private Random random;

  public AnimalThread(Animal animal) {
    this.animal = animal;
    this.random = new Random();
  }

  public void run() {
    while(true) {
      int i = random.nextInt(10);
      switch (i) {
        case 0:
        case 1:
        case 2:
          animal.purr();
          break;
        case 3:
        case 4:
        case 5:
        case 6:
          animal.mew();
          break;
        case 7:
        case 8:
          animal.eat();
          break;
        case 9:
          animal.piss();
          break;
        default:
          continue;
      }
      try {
        TimeUnit.SECONDS.sleep(5);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
