package ru.x5;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Data
@Accessors(chain = true)
public class Keeper implements Runnable {
  private String name;
  private List<Animal> animals;

  public void run() {
    Random random = new Random();
    while (true) {
      int i = random.nextInt(animals.size());
      System.out.println(name + " гладит кошечку по имени " + animals.get(i).getName());

      try {
        TimeUnit.SECONDS.sleep(10);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
