package ru.x5;

public interface Animal {
  public Animal getAnimal();
  public String getName();
  public void mew();
  public void purr();
  public void eat();
  public void piss();
}
