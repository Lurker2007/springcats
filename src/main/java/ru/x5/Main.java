package ru.x5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan(basePackages = {"ru.x5"})
public class Main {
  public static void main(String[] args) {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    context.register(Main.class);
    context.refresh();
    ShelterService shelterService = context.getBean("shelterService", ShelterService.class);
    shelterService.liveInShelter();
  }

  @Autowired
  AnimalGenerator animalGenerator;

  @Bean
  public Keeper getKeeper() {
    return new Keeper().setName("Анжела-смотритель").setAnimals(animalGenerator.getAnimals());
  }
}
