package ru.x5;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TestAspect {
  private String name = "Тетя Глаша";

  @After("execution(public * getAnimal())")
  public void clean() {
    System.out.println(name + ": убирает за кошечкой");
  }
}