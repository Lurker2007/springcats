package ru.x5;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@Data
@Accessors(chain = true)
@Component
@Scope("prototype")
public class Cat implements Animal{
    private String name;
    private Random random = new Random();

    private int i = 0;

    public Cat() {
    }

    public Animal getAnimal() {
        int i = random.nextInt(CatData.names.length);
        this.name = CatData.names[i];
        return new Cat().setName(name);
    }

    public void mew() {
        System.out.println(name + ": Мяу");
    }

    public void purr() {
        System.out.println(name + ": Мрррр");
    }

    public void eat() {
        System.out.println(name + ": Ням ням");
    }

    public void piss() {
        System.out.println(name + ": Пи пи");
    }
}
